package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type ReadInput func(string) []DigTask

type Coordinates struct {
	x int
	y int
}

type Direction struct {
	dx int
	dy int
}

func (d Direction) Multiple(m int) Direction {
	return Direction{m * d.dx, m * d.dy}
}

func (c Coordinates) AddDirection(d Direction) Coordinates {
	return Coordinates{c.x + d.dx, c.y + d.dy}
}

var NORTH = Direction{0, -1}
var SOUTH = Direction{0, 1}
var WEST = Direction{-1, 0}
var EAST = Direction{1, 0}

var DIRECTIONS = map[string]Direction{
	"U": NORTH,
	"D": SOUTH,
	"L": WEST,
	"R": EAST,
}

var DIRECTIONS_HEX = map[int]Direction{
	0: DIRECTIONS["R"],
	1: DIRECTIONS["D"],
	2: DIRECTIONS["L"],
	3: DIRECTIONS["U"],
}

type DigTask struct {
	direction Direction
	distance  int
}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part(string(input), read_input_incorrect)
	part(string(input), read_input_correct)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part(input string, read_input ReadInput) {
	dig_plan := read_input(input)
	outline := get_edge(dig_plan)

	area := get_area(outline)

	fmt.Println("How many cubic meters of lava could the lagoon hold?", area)
}

func get_edge(dig_plan []DigTask) []Coordinates {
	outline := []Coordinates{{0, 0}}

	for _, dig_task := range dig_plan {
		start_vertex := outline[len(outline)-1]
		end_vertex := start_vertex.AddDirection(dig_task.direction.Multiple(dig_task.distance))
		outline = append(outline, end_vertex)
	}

	if outline[len(outline)-1] != outline[0] {
		panic("dig plan does not make a loop")
	}

	return outline[:len(outline)-1]
}

func get_area(outline []Coordinates) int {
	// combination of Shoelace formula and Pick's theorem
	// https://en.wikipedia.org/wiki/Shoelace_formula
	// https://en.wikipedia.org/wiki/Pick%27s_theorem

	sum_determinants := 0
	sum_boundary_points := 0

	for i := 0; i < len(outline); i++ {
		p0 := outline[i]
		p1 := outline[(i+1)%len(outline)]
		sum_determinants += p0.x*p1.y - p0.y*p1.x
		sum_boundary_points += intabs(p1.x-p0.x) + intabs(p1.y-p0.y)
	}

	area := sum_determinants/2 + sum_boundary_points/2 + 1

	return area
}

func read_input_incorrect(input string) []DigTask {
	lines := strings.Split(strings.TrimSpace(input), "\r\n")

	re := regexp.MustCompile(`([UDLR]) (\d+)`)

	dig_plan := []DigTask{}

	for _, line := range lines {
		match := re.FindStringSubmatch(line)
		dig_plan = append(dig_plan, DigTask{
			direction: DIRECTIONS[match[1]],
			distance:  string_to_int(match[2]),
		})
	}

	return dig_plan
}

func read_input_correct(input string) []DigTask {
	lines := strings.Split(strings.TrimSpace(input), "\r\n")

	re := regexp.MustCompile(`([\da-f]{5})([\da-f])`)

	dig_plan := []DigTask{}

	for _, line := range lines {
		match := re.FindStringSubmatch(line)
		dig_plan = append(dig_plan, DigTask{
			direction: DIRECTIONS_HEX[string_to_int(match[2])],
			distance:  hex_string_to_int(match[1]),
		})
	}

	return dig_plan
}

func string_to_int(str string) int {
	i, err := strconv.Atoi(str)
	check(err)
	return i
}

func hex_string_to_int(str string) int {
	i, err := strconv.ParseInt(str, 16, 0)
	check(err)
	return int(i)
}

func intabs(x int) int {
	if x >= 0 {
		return x
	} else {
		return -x
	}
}
