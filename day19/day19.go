package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Part map[string]int

func (part Part) RatingSum() int {
	sum := 0
	for _, v := range part {
		sum += v
	}
	return sum
}

type Parts []Part

type WorkflowStep struct {
	next     string
	category string
	operator string
	value    int
}

type Workflow []WorkflowStep

type Workflows map[string]Workflow

type cmp func(int, int) bool

func lt(a int, b int) bool {
	return a < b
}

func gt(a int, b int) bool {
	return a > b
}

var operators = map[string]cmp{
	"<": lt,
	">": gt,
}

type RatingRange struct {
	lower int // inclusive
	upper int // non-inclusive
}

type PartRatingRange map[string]RatingRange

func (part_rating_range PartRatingRange) Copy() PartRatingRange {
	return PartRatingRange{
		"x": part_rating_range["x"],
		"m": part_rating_range["m"],
		"a": part_rating_range["a"],
		"s": part_rating_range["s"],
	}
}

func (part_rating_range PartRatingRange) GetSize() int {
	size := 1
	for _, rating_range := range part_rating_range {
		size *= rating_range.upper - rating_range.lower
	}
	return size
}

func (workflows Workflows) EvaluatePart(part Part) bool {
	workflow_name := BEGIN_WORKFLOW_NAME

	for !(workflow_name == ACCEPTED || workflow_name == REJECTED) {
		workflow_name = workflows[workflow_name].EvaluatePart(part)
	}

	return workflow_name == ACCEPTED
}

func (workflow Workflow) EvaluatePart(part Part) string {
	for _, step := range workflow {
		if step.EvaluatePart(part) {
			return step.next
		}
	}
	panic("this shouldn't happen")
}

func (workflow_step WorkflowStep) EvaluatePart(part Part) bool {
	if workflow_step.category == "" { // always follow target
		return true
	}
	cmp_func := operators[workflow_step.operator]
	part_value := part[workflow_step.category]
	return cmp_func(part_value, workflow_step.value)
}

const ACCEPTED string = "A"
const REJECTED string = "R"

const BEGIN_WORKFLOW_NAME string = "in"

var BEGIN_PART_RATING_RANGE PartRatingRange = PartRatingRange{
	"x": RatingRange{lower: 1, upper: 4001},
	"m": RatingRange{lower: 1, upper: 4001},
	"a": RatingRange{lower: 1, upper: 4001},
	"s": RatingRange{lower: 1, upper: 4001},
}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part1(string(input))
	part2(string(input))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part1(input string) {
	workflows, parts := read_input(input)

	rating_sum := 0

	for _, part := range parts {
		if workflows.EvaluatePart(part) {
			rating_sum += part.RatingSum()
		}
	}

	fmt.Println("What do you get if you add together all of the rating numbers for all of the parts that ultimately get accepted?", rating_sum)
}

func part2(input string) {
	// the graph does not contain cycles,
	// and every workflow only has one input

	workflows, _ := read_input(input)

	// keep track of the ranges of ratings that lead to a given workflow
	part_rating_range_map := map[string][]PartRatingRange{}

	// all ratings lead to the begin workflow
	part_rating_range_map[BEGIN_WORKFLOW_NAME] =
		[]PartRatingRange{BEGIN_PART_RATING_RANGE}

	// keep track of which workflows we need to check
	// we can do this in this way because the graph does not contain cycles
	workflows_to_check := []string{BEGIN_WORKFLOW_NAME}

	for len(workflows_to_check) > 0 {
		workflow_name := workflows_to_check[0]
		workflows_to_check = workflows_to_check[1:]

		workflow := workflows[workflow_name]
		part_rating_ranges := part_rating_range_map[workflow_name]

		for _, workflow_step := range workflow {
			// for every step where the range is split, keep track of the part
			// that continues to the next step, and the part that goes to a
			// different workflow
			category := workflow_step.category

			if workflow_step.category == "" {
				// all values in the range go to the final target
				part_rating_range_map[workflow_step.next] = append(part_rating_range_map[workflow_step.next],
					part_rating_ranges...)
			} else {
				for i := range part_rating_ranges {
					split := split_rating_range(part_rating_ranges[i][category],
						workflow_step)

					new_rating_range := part_rating_ranges[i].Copy()

					// part that goes to the other workflow
					new_rating_range[category] = split[0]

					// part that continues in this workflow
					part_rating_ranges[i][category] = split[1]

					part_rating_range_map[workflow_step.next] = append(part_rating_range_map[workflow_step.next], new_rating_range)

				}
			}

			if !(workflow_step.next == ACCEPTED || workflow_step.next == REJECTED) {
				// mark the other workflow as one to check
				// we don't need to check whether it is already in this list as
				// as every workflow only has a single input
				workflows_to_check = append(workflows_to_check, workflow_step.next)
			}
		}
	}

	sum := 0

	for _, part_rating_range := range part_rating_range_map[ACCEPTED] {
		sum += part_rating_range.GetSize()
	}

	fmt.Println("How many distinct combinations of ratings will be accepted by the Elves' workflows?", sum)
}

func split_rating_range(rating_range RatingRange, workflow_step WorkflowStep) []RatingRange {
	// this assumes that `rating_range` belongs to the category specified in
	// `workflow_step.category`

	// the first range to be returned is the one where the workflow rule is
	// matched, the second range to be returned is the one where the workflow
	// rule is not matched
	if workflow_step.operator == "<" {
		threshold := clamp(workflow_step.value, rating_range.lower, rating_range.upper)
		return []RatingRange{
			{
				lower: rating_range.lower,
				upper: threshold,
			},
			{
				lower: threshold,
				upper: rating_range.upper,
			},
		}
	} else if workflow_step.operator == ">" {
		threshold := clamp(workflow_step.value+1, rating_range.lower, rating_range.upper)
		return []RatingRange{
			{
				lower: threshold,
				upper: rating_range.upper,
			},
			{
				lower: rating_range.lower,
				upper: threshold,
			},
		}
	} else {
		panic("this shouldn't happen")
	}
}

func read_input(input string) (Workflows, Parts) {
	input_parts := strings.Split(strings.TrimSpace(input), "\r\n\r\n")

	workflows_str, parts_str := input_parts[0], input_parts[1]

	workflows := read_workflows(workflows_str)
	parts := read_parts(parts_str)

	return workflows, parts
}

func read_workflows(workflows_str string) Workflows {
	lines := strings.Split(workflows_str, "\r\n")

	workflow_re := regexp.MustCompile(`([a-z]+){(.+)}`)
	step_re := regexp.MustCompile(`(?:([xmas])([<>])(\d+):)?([a-zAR]+)`)

	workflows := Workflows{}

	for _, line := range lines {
		workflow_match := workflow_re.FindStringSubmatch(line)
		name := workflow_match[1]
		step_str := workflow_match[2]

		step_matches := step_re.FindAllStringSubmatch(step_str, -1)

		workflow := make(Workflow, len(step_matches))

		for i, step_match := range step_matches {
			step := WorkflowStep{
				next:     step_match[4],
				category: step_match[1],
				operator: step_match[2],
				value:    string_to_int(step_match[3]),
			}

			workflow[i] = step
		}

		workflows[name] = workflow
	}

	return workflows
}

func read_parts(parts_str string) Parts {
	lines := strings.Split(parts_str, "\r\n")

	part_re := regexp.MustCompile(`([xmas])=(\d+)`)

	parts := make(Parts, len(lines))

	for i, line := range lines {
		matches := part_re.FindAllStringSubmatch(line, -1)

		part := Part{}

		for _, match := range matches {
			category := match[1]
			value := string_to_int(match[2])
			part[category] = value
		}

		parts[i] = part
	}

	return parts
}

func string_to_int(str string) int {
	if str == "" {
		return 0
	}
	i, err := strconv.Atoi(str)
	check(err)
	return i
}

func clamp(v, a, b int) int {
	return max(a, min(v, b))
}
