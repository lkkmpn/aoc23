package main

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

type node struct {
	left  string
	right string
}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part1(string(input))
	part2(string(input))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part1(input string) {
	instructions, network := parse_input_data(input)

	current_node := "AAA"
	steps := 0

	for current_node != "ZZZ" {
		instruction := instructions[steps%len(instructions)]
		current_node = next_node(network[current_node], instruction)
		steps++
	}

	fmt.Println("How many steps are required to reach ZZZ?", steps)
}

func part2(input string) {
	instructions, network := parse_input_data(input)

	// a single node ..A always ends up at the same node ..Z, and this cycles,
	// so we can look at the number of steps it takes for this cycle to occur
	// and take the least common multiple of all cycles

	start_nodes := get_all_start_nodes(network)
	steps_per_start_node := make([]int, len(start_nodes))

	for i, start_node := range start_nodes {
		current_node := start_node
		steps := 0

		for current_node[2] != 'Z' {
			instruction := instructions[steps%len(instructions)]
			current_node = next_node(network[current_node], instruction)
			steps++
		}

		steps_per_start_node[i] = steps
	}

	steps := lcmm(steps_per_start_node)

	fmt.Println("How many steps does it take before you're only on nodes that end with Z?", steps)
}

func next_node(current_node node, instruction byte) string {
	if instruction == 'L' {
		return current_node.left
	} else if instruction == 'R' {
		return current_node.right
	} else {
		panic("this shouldn't happen")
	}
}

func get_all_start_nodes(network map[string]node) []string {
	start_nodes := []string{}
	for node := range network {
		if node[2] == 'A' {
			start_nodes = append(start_nodes, node)
		}
	}
	return start_nodes
}

func parse_input_data(input string) (string, map[string]node) {
	lines := strings.Split(strings.TrimSpace(input), "\r\n")

	re := regexp.MustCompile(`\w+`)

	instructions := lines[0]

	network := map[string]node{}
	for _, line := range lines[2:] {
		matches := re.FindAllString(line, -1)
		if len(matches) != 3 {
			panic("this shouldn't happen")
		}
		current_node := matches[0]
		next_node := node{left: matches[1], right: matches[2]}
		network[current_node] = next_node
	}

	return instructions, network
}

func gcd(a int, b int) int {
	// https://en.wikipedia.org/wiki/Euclidean_algorithm#Implementations
	if b == 0 {
		return a
	} else {
		return gcd(b, a%b)
	}
}

func lcm(a int, b int) int {
	// https://en.wikipedia.org/wiki/Least_common_multiple#Using_the_greatest_common_divisor
	if a == 0 && b == 0 {
		return 0
	} else {
		return a * (b / gcd(a, b))
	}
}

func lcmm(vals []int) int {
	if len(vals) < 2 {
		panic("this shouldn't happen")
	}
	running_lcm := lcm(vals[0], vals[1])
	for _, val := range vals[2:] {
		running_lcm = lcm(running_lcm, val)
	}
	return running_lcm
}
