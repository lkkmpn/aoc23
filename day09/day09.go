package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part(string(input), 1)
	part(string(input), 2)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part(input string, part int) {
	sequences := read_input(input)

	sum := 0

	for _, sequence := range sequences {
		sum += get_extrapolated_value(sequence, part)
	}

	fmt.Println("What is the sum of these extrapolated values?", sum)
}

func get_extrapolated_value(sequence []int, part int) int {
	// create sequences of differences
	sequences := [][]int{sequence}
	for !is_sequence_zeros(sequences[len(sequences)-1]) {
		sequences = append(sequences, get_diff_sequence(sequences[len(sequences)-1]))
	}

	// extrapolate next value
	extrapolated_value := 0
	for i := len(sequences) - 1; i >= 0; i-- {
		switch part {
		case 1:
			extrapolated_value = extrapolated_value + sequences[i][len(sequences[i])-1]
		case 2:
			extrapolated_value = -extrapolated_value + sequences[i][0]
		}
	}

	return extrapolated_value
}

func get_diff_sequence(sequence []int) []int {
	diff := make([]int, len(sequence)-1)
	for i := 0; i < len(sequence)-1; i++ {
		diff[i] = sequence[i+1] - sequence[i]
	}
	return diff
}

func is_sequence_zeros(sequence []int) bool {
	for _, v := range sequence {
		if v != 0 {
			return false
		}
	}
	return true
}

func read_input(input string) [][]int {
	lines := strings.Split(strings.TrimSpace(input), "\r\n")
	sequences := make([][]int, len(lines))

	for i, line := range lines {
		numbers := strings.Split(line, " ")
		sequence := make([]int, len(numbers))
		for j, number := range numbers {
			sequence[j] = string_to_int(number)
		}
		sequences[i] = sequence
	}

	return sequences
}

func string_to_int(str string) int {
	v, err := strconv.Atoi(str)
	check(err)
	return v
}
