package main

import (
	"fmt"
	"math"
	"os"
	"strings"
)

type pipe_tile map[[2]int]bool // [2]int: {y, x}

var NORTH = [2]int{-1, 0}
var SOUTH = [2]int{1, 0}
var WEST = [2]int{0, -1}
var EAST = [2]int{0, 1}
var START = [2]int{0, 0} // special case

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part1(string(input))
	part2(string(input))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part1(input string) {
	grid, start_point := read_input(input)

	loop_points := get_loop_points(grid, start_point)

	loop_length := len(loop_points)
	farthest_point := loop_length / 2

	fmt.Println("How many steps along the loop does it take to get from the starting position to the point farthest from the starting position?", farthest_point)
}

func part2(input string) {
	grid, start_point := read_input(input)

	loop_points := get_loop_points(grid, start_point)

	ny, nx := len(grid), len(grid[0])
	num_inside := 0

	for i := range make_range(0, ny) {
		for j := range make_range(0, nx) {
			point := [2]int{i, j}
			inside := get_inside(grid, loop_points, point)
			if inside {
				num_inside++
			}
		}
	}

	fmt.Println("How many tiles are enclosed by the loop?", num_inside)
}

func get_loop_points(grid [][]pipe_tile, start_point [2]int) [][2]int {
	direction := get_any_direction(grid[start_point[0]][start_point[1]])

	current_point := start_point
	point := [][2]int{}

	for len(point) == 0 || current_point != start_point {
		current_point[0] += direction[0]
		current_point[1] += direction[1]
		direction = get_next_direction(grid[current_point[0]][current_point[1]], direction)
		point = append(point, current_point)
	}

	return point
}

func get_inside(grid [][]pipe_tile, loop_points [][2]int, point [2]int) bool {
	// point is not inside if point is on loop
	for _, loop_point := range loop_points {
		if loop_point == point {
			return false
		}
	}

	// nonzero winding number means the point is inside the loop
	winding_number := get_winding_number(loop_points, point)
	return winding_number != 0
}

func get_winding_number(loop_points [][2]int, point [2]int) int {
	angle := 0.0

	for i := range loop_points {
		// get angle formed by `p0` -- `point` -- `p1`
		p0 := loop_points[i]
		p1 := loop_points[(i+1)%len(loop_points)]

		// define vectors from `point` to `p0` and `p1` respectively
		v0 := [2]float64{float64(p0[0] - point[0]), float64(p0[1] - point[1])}
		v1 := [2]float64{float64(p1[0] - point[0]), float64(p1[1] - point[1])}

		// use atan2 to get the angles from `v0` and `v1` to the x+ axis
		// this automatically deals with any sign issues that might arise when
		// using acos
		da := math.Atan2(v1[0], v1[1]) - math.Atan2(v0[0], v0[1])

		// normalize angle to (−π, +π]
		if da > math.Pi {
			da -= 2 * math.Pi
		} else if da <= -math.Pi {
			da += 2 * math.Pi
		}
		angle += da
	}

	return int(math.Round(angle / 2 / math.Pi))
}

func get_any_direction(tile pipe_tile) [2]int {
	// return the first key in `tile`, order doesn't matter
	for k := range tile {
		return k
	}
	panic("this shouldn't happen")
}

func get_next_direction(tile pipe_tile, prev_direction [2]int) [2]int {
	// following the inverse of `prev_direction` would lead us back where we
	// came from, so we take the other key in `tile`
	same_direction := [2]int{-prev_direction[0], -prev_direction[1]}
	for k := range tile {
		if k != same_direction {
			return k
		}
	}
	panic("this shouldn't happen")
}

func read_input(input string) ([][]pipe_tile, [2]int) {
	lines := strings.Split(strings.TrimSpace(input), "\r\n")

	grid := [][]pipe_tile{}
	start_point := [2]int{}

	// read grid from input
	for i, line := range lines {
		row := []pipe_tile{}
		for j, char := range line {
			tile := char_to_tile(char)
			if tile[START] {
				start_point = [2]int{i, j}
			}
			row = append(row, tile)
		}
		grid = append(grid, row)
	}

	// resolve start point
	grid[start_point[0]][start_point[1]] = resolve_start_point(grid, start_point)

	return grid, start_point
}

func char_to_tile(char rune) pipe_tile {
	switch char {
	case '|':
		return pipe_tile{NORTH: true, SOUTH: true}
	case '-':
		return pipe_tile{WEST: true, EAST: true}
	case 'L':
		return pipe_tile{NORTH: true, EAST: true}
	case 'J':
		return pipe_tile{NORTH: true, WEST: true}
	case '7':
		return pipe_tile{SOUTH: true, WEST: true}
	case 'F':
		return pipe_tile{SOUTH: true, EAST: true}
	case '.':
		return pipe_tile{}
	case 'S':
		// directions are dealt with later
		return pipe_tile{START: true}
	default:
		panic("this shouldn't happen")
	}
}

func resolve_start_point(grid [][]pipe_tile, start_point [2]int) pipe_tile {
	north, south, west, east := false, false, false, false
	sy, sx := start_point[0], start_point[1]
	ny, nx := len(grid), len(grid[0])

	// set this pipe by neighboring pipe openings (if they exist)
	// there's only one solution, because S will have exactly two pipes
	// connecting to it
	if sy > 0 {
		north = grid[sy-1][sx][SOUTH]
	}
	if sy < ny-1 {
		south = grid[sy+1][sx][NORTH]
	}
	if sx > 0 {
		west = grid[sy][sx-1][EAST]
	}
	if sx < nx-1 {
		east = grid[sy][sx+1][WEST]
	}

	tile := pipe_tile{}
	if north {
		tile[NORTH] = true
	}
	if south {
		tile[SOUTH] = true
	}
	if west {
		tile[WEST] = true
	}
	if east {
		tile[EAST] = true
	}

	if len(tile) != 2 {
		panic("this shouldn't happen")
	}

	return tile
}

func make_range(min, max int) []int {
	// https://stackoverflow.com/a/39868255
	a := make([]int, max-min+1)
	for i := range a {
		a[i] = min + i
	}
	return a
}
