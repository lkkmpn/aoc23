package main

import (
	"fmt"
	"os"
	"regexp"
	"slices"
	"strings"
)

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part(string(input), 2)
	part(string(input), 1000000)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part(input string, scale int) {
	rows, columns := read_input(input)

	rows = expand_galaxy(rows, scale)
	columns = expand_galaxy(columns, scale)

	if len(rows) != len(columns) {
		panic("this shouldn't happen")
	}

	sum := 0

	for i := 0; i < len(rows); i++ {
		for j := i + 1; j < len(rows); j++ {
			sum += taxicab_distance(rows[i], columns[i], rows[j], columns[j])
		}
	}

	fmt.Println("What is the sum of these lengths?", sum)
}

func expand_galaxy(coordinates []int, scale int) []int {
	max_coordinate := slices.Max(coordinates)

	expand := []int{}

	// we don't need to include max_coordinate because that contains a galaxy
	for coordinate := range make_range(0, max_coordinate) {
		if !slices.Contains(coordinates, coordinate) {
			expand = append(expand, coordinate)
		}
	}

	// expand in backwards order
	for expand_i := len(expand) - 1; expand_i >= 0; expand_i-- {
		for coordinate_i := range coordinates {
			if coordinates[coordinate_i] > expand[expand_i] {
				coordinates[coordinate_i] += scale - 1
			}
		}
	}

	return coordinates
}

func taxicab_distance(y0 int, x0 int, y1 int, x1 int) int {
	return intabs(y1-y0) + intabs(x1-x0)
}

func intabs(x int) int {
	if x >= 0 {
		return x
	} else {
		return -x
	}
}

func read_input(input string) ([]int, []int) {
	re := regexp.MustCompile(`#`)
	rows, columns := []int{}, []int{}

	lines := strings.Split(strings.TrimSpace(input), "\r\n")

	for i, line := range lines {
		galaxy_indices := re.FindAllStringIndex(line, -1)
		for _, j := range galaxy_indices {
			rows = append(rows, i)
			columns = append(columns, j[0])
		}
	}

	return rows, columns
}

func make_range(min, max int) []int {
	// https://stackoverflow.com/a/39868255
	a := make([]int, max-min+1)
	for i := range a {
		a[i] = min + i
	}
	return a
}
