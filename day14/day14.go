package main

import (
	"fmt"
	"os"
	"regexp"
	"sort"
	"strings"
)

type rock_shape int

const (
	ROUND rock_shape = iota
	CUBE  rock_shape = iota
)

type rock_container struct {
	coordinates [2]int
	shape       rock_shape
}

type direction_type struct {
	index           int
	order_direction int
}

var NORTH = direction_type{1, 1}
var SOUTH = direction_type{1, -1}
var WEST = direction_type{0, 1}
var EAST = direction_type{0, -1}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part1(string(input))
	part2(string(input))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part1(input string) {
	rocks, size := read_input(input)
	rocks = move_rocks_in_direction(rocks, size, NORTH)
	load := get_total_load(rocks, size[1])

	fmt.Println("What is the total load on the north support beams?", load)
}

func part2(input string) {
	cycles := 1000000000

	rocks, size := read_input(input)

	// we don't have to run through all those cycles; at some point, we enter
	// a loop where the rocks are at the same location
	// we find the length of this loop and then fast-forward to the end
	visited := map[string]int{}
	loads := []int{}

	it := 0
	var loop_length int

	for {
		rocks = move_rocks_cycle(rocks, size)
		loads = append(loads, get_total_load(rocks, size[1]))
		repr := rocks_to_repr(rocks)
		if v, ok := visited[repr]; ok {
			// we have seen this configuration already, so we are looping
			loop_length = it - v
			break
		}
		visited[repr] = it
		it += 1
	}

	// offset for the step we already did in the new loop
	uncycled_steps := (cycles - it - 1) % loop_length
	load := loads[len(loads)-1-loop_length+uncycled_steps]

	fmt.Println("What is the total load on the north support beams?", load)
}

func get_total_load(rocks []rock_container, y_size int) int {
	load := 0

	for _, rock := range rocks {
		load += get_load(rock, y_size)
	}

	return load
}

func get_load(rock rock_container, y_size int) int {
	if rock.shape == ROUND {
		return y_size - rock.coordinates[1]
	} else {
		return 0
	}
}

func move_rocks_cycle(rocks []rock_container, size [2]int) []rock_container {
	cycle := [...]direction_type{NORTH, WEST, SOUTH, EAST}

	for _, direction := range cycle {
		rocks = move_rocks_in_direction(rocks, size, direction)
	}

	return rocks
}

func move_rocks_in_direction(rocks []rock_container, size [2]int, direction direction_type) []rock_container {
	// keep track of the position of the last rock (or wall), and move rocks
	// incrementally against that position
	rocks = get_rocks_in_direction(rocks, direction)
	other_index := get_other_index(direction.index)
	initial_last_rock_position := get_initial_last_rock_position(direction, size)

	last_rock_positions := map[int]int{}
	for i := 0; i < size[other_index]; i++ {
		last_rock_positions[i] = initial_last_rock_position
	}

	for i := range rocks {
		if rocks[i].shape == ROUND {
			// roll rock to the next available spot
			rocks[i].coordinates[direction.index] =
				last_rock_positions[rocks[i].coordinates[other_index]] + direction.order_direction
		}
		// set occupied spot
		last_rock_positions[rocks[i].coordinates[other_index]] =
			rocks[i].coordinates[direction.index]
	}

	return rocks
}

func get_rocks_in_direction(rocks []rock_container, direction direction_type) []rock_container {
	// sort the rocks such that looping over them gives the desired ordering,
	// with the rock closest to the direction the platform is angled to first
	sort.Slice(rocks, func(i int, j int) bool {
		return get_sort_coordinate(rocks[i], direction) <
			get_sort_coordinate(rocks[j], direction)
	})

	return rocks
}

func get_sort_coordinate(rock rock_container, direction direction_type) int {
	return rock.coordinates[direction.index] * direction.order_direction
}

func get_other_index(index int) int {
	// 0 -> 1, 1 -> 0
	return (index + 1) % 2
}

func get_initial_last_rock_position(direction direction_type, size [2]int) int {
	if direction.order_direction == 1 { // NORTH or WEST
		return -1
	} else { // SOUTH or EAST
		return size[direction.index]
	}
}

func read_input(input string) ([]rock_container, [2]int) {
	round_re := regexp.MustCompile(`O`)
	cube_re := regexp.MustCompile(`#`)

	rocks := []rock_container{}

	lines := strings.Split(strings.TrimSpace(input), "\r\n")

	for i, line := range lines {
		round_rocks := round_re.FindAllStringIndex(line, -1)
		for _, j := range round_rocks {
			rocks = append(rocks,
				rock_container{coordinates: [2]int{j[0], i}, shape: ROUND},
			)
		}

		cube_rocks := cube_re.FindAllStringIndex(line, -1)
		for _, j := range cube_rocks {
			rocks = append(rocks,
				rock_container{coordinates: [2]int{j[0], i}, shape: CUBE},
			)
		}
	}

	size := [2]int{len(lines[0]), len(lines)}

	return rocks, size
}

func rocks_to_repr(rocks []rock_container) string {
	// probably very suboptimal string manipulation

	// sort rocks in a fixed order
	sort.Slice(rocks, func(i int, j int) bool {
		if rocks[i].coordinates[0] != rocks[j].coordinates[0] {
			return rocks[i].coordinates[0] < rocks[j].coordinates[0]
		}
		return rocks[i].coordinates[1] < rocks[j].coordinates[1]
	})

	rock_strings := make([]string, len(rocks))
	for i, rock := range rocks {
		rock_strings[i] = fmt.Sprintf("%d,%d", rock.coordinates[0], rock.coordinates[1])
	}

	return strings.Join(rock_strings, ";")
}
