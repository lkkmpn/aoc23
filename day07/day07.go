package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

const n_cards int = 13

type hand_data struct {
	hand  string
	bid   int
	score int
}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part(string(input), 1)
	part(string(input), 2)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part(input string, part int) {
	hands := get_hand_data(input)

	for i := range hands {
		hands[i].score += 1000000 * score_hand_type(hands[i].hand, part)
		hands[i].score += score_hand_card_order(hands[i].hand, part)
	}

	// sort by score
	sort.Slice(hands, func(i int, j int) bool {
		return hands[i].score < hands[j].score
	})

	// calculate winnings
	winnings := 0
	for i, hand := range hands {
		winnings += (i + 1) * hand.bid
	}

	fmt.Println("What are the total winnings?", winnings)
}

func score_hand_type(hand string, part int) int {
	// ======================
	// hand type        score
	// ----------------------
	// five of a kind     7
	// four of a kind     6
	// full house         5
	// three of a kind    4
	// two pair           3
	// one pair           2
	// high card          1
	// ======================

	// count how often each card occurs
	occurrence_count := map[rune]int{}
	for _, card := range hand {
		occurrence_count[card]++
	}

	if part == 2 {
		// now that J is a joker, we can add the number of joker occurrences to
		// the highest number of occurrences to get the best possible hand
		occurrence_joker := occurrence_count['J']
		occurrence_count['J'] = 0

		var card_highest_occurrence rune
		highest_occurrence := 0
		for card, occurrence := range occurrence_count {
			if occurrence > highest_occurrence {
				card_highest_occurrence = card
				highest_occurrence = occurrence
			}
		}

		occurrence_count[card_highest_occurrence] += occurrence_joker
	}

	// count how often each count occurs
	meta_count := map[int]int{}
	for _, count := range occurrence_count {
		meta_count[count]++
	}

	if meta_count[5] == 1 {
		return 7
	} else if meta_count[4] == 1 {
		return 6
	} else if meta_count[3] == 1 && meta_count[2] == 1 {
		return 5
	} else if meta_count[3] == 1 {
		return 4
	} else if meta_count[2] == 2 {
		return 3
	} else if meta_count[2] == 1 {
		return 2
	} else {
		return 1
	}
}

func score_hand_card_order(hand string, part int) int {
	// max value: 371293 (n_cards ** len(hand))
	score := 0
	for i, card := range hand {
		score += intpow(n_cards, len(hand)-i-1) * get_card_strength(card, part)
	}
	return score
}

func get_hand_data(input string) []hand_data {
	lines := strings.Split(strings.TrimSpace(input), "\r\n")

	hand_bids := make([]hand_data, len(lines))
	for i, line := range lines {
		parts := strings.Split(line, " ")
		hand_str := parts[0]
		bid_str := parts[1]
		bid, err := strconv.Atoi(bid_str)
		check(err)
		hand_bids[i] = hand_data{
			hand: hand_str,
			bid:  bid,
		}
	}

	return hand_bids
}

func get_card_strength(card rune, part int) int {
	var strengths string // low to high
	if part == 1 {
		strengths = "23456789TJQKA"
	} else {
		strengths = "J23456789TQKA"
	}

	for i, other := range strengths {
		if card == other {
			return i
		}
	}
	panic("this shouldn't happen")
}

func intpow(b int, x int) int {
	res := 1
	for i := 0; i < x; i++ {
		res *= b
	}
	return res
}
