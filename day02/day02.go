package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	input, err := os.ReadFile("input.txt")

	if err != nil {
		panic(err)
	}

	part(string(input))
}

func part(input string) {
	max_red := 12
	max_green := 13
	max_blue := 14

	lines := strings.Split(input, "\r\n")

	sum_ids := 0
	sum_powers := 0

	for _, line := range lines {
		if line == "" {
			continue
		}

		first_parts := strings.Split(line, ": ")
		game_id, err := strconv.Atoi(first_parts[0][5:])

		if err != nil {
			panic(err)
		}

		max_red_seen := 0
		max_green_seen := 0
		max_blue_seen := 0

		// we don't actually need to consider the subsets individually
		cubes_revealed_strs := strings.Split(strings.ReplaceAll(first_parts[1], ";", ","), ", ")

		for _, cubes_revealed_str := range cubes_revealed_strs {
			parts := strings.Split(cubes_revealed_str, " ")
			num_cubes, err := strconv.Atoi(parts[0])

			if err != nil {
				panic(err)
			}

			if parts[1] == "red" && num_cubes > max_red_seen {
				max_red_seen = num_cubes
			}
			if parts[1] == "green" && num_cubes > max_green_seen {
				max_green_seen = num_cubes
			}
			if parts[1] == "blue" && num_cubes > max_blue_seen {
				max_blue_seen = num_cubes
			}
		}

		if max_red_seen <= max_red && max_green_seen <= max_green && max_blue_seen <= max_blue {
			// game could have been possible
			sum_ids += game_id
		}

		sum_powers += max_red_seen * max_green_seen * max_blue_seen
	}

	fmt.Println("What is the sum of the IDs of those games?", sum_ids)
	fmt.Println("What is the sum of the power of these sets?", sum_powers)
}
