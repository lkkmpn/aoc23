package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type step_type struct {
	lens      lens_type
	operation string
}

type lens_type struct {
	label        string
	focal_length int
}

type boxes_type map[int][]lens_type

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part1(string(input))
	part2(string(input))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part1(input string) {
	steps := read_input_simple(input)

	sum := 0

	for _, step := range steps {
		sum += get_hash(step)
	}

	fmt.Println("What is the sum of the results?", sum)
}

func part2(input string) {
	steps := read_input_regex(input)

	boxes := boxes_type{}

	for _, step := range steps {
		boxes = do_step(boxes, step)
	}

	sum := 0

	for box_idx, lenses := range boxes {
		for slot_idx, lens := range lenses {
			sum += get_focusing_power(box_idx, slot_idx, lens.focal_length)
		}
	}

	fmt.Println("What is the focusing power of the resulting lens configuration?", sum)
}

func get_hash(str string) int {
	current_value := 0

	for _, c := range str {
		current_value = 17 * (current_value + int(c)) % 256
	}

	return current_value
}

func do_step(boxes boxes_type, step step_type) boxes_type {
	box_index := get_hash(step.lens.label)
	box := boxes[box_index]

	switch step.operation {
	case "-":
		for i := range box {
			if box[i].label == step.lens.label {
				// remove lens with this label
				box = append(box[:i], box[i+1:]...)
				break
			}
		}
	case "=":
		lens_found := false
		for i := range box {
			if box[i].label == step.lens.label {
				// replace this lens
				box[i] = step.lens
				lens_found = true
				break
			}
		}
		if !lens_found {
			// append this lens
			box = append(box, step.lens)
		}
	default:
		panic("this shouldn't happen")
	}

	boxes[box_index] = box

	return boxes
}

func get_focusing_power(box_idx int, slot_idx int, focal_length int) int {
	return (box_idx + 1) * (slot_idx + 1) * focal_length
}

func read_input_simple(input string) []string {
	return strings.Split(strings.TrimSpace(input), ",")
}

func read_input_regex(input string) []step_type {
	re := regexp.MustCompile(`([a-z]+)([=-])(\d)?`)

	matches := re.FindAllStringSubmatch(input, -1)
	steps := make([]step_type, len(matches))

	for i, match := range matches {
		label, operation, focal_length_str := match[1], match[2], match[3]
		focal_length := get_focal_length(focal_length_str)
		lens := lens_type{label: label, focal_length: focal_length}
		steps[i] = step_type{
			lens:      lens,
			operation: operation,
		}
	}

	return steps
}

func get_focal_length(focal_length_str string) int {
	if focal_length_str == "" {
		// this only happens if the operation is "-",
		// and we don't need this value
		return 0
	}
	focal_length, err := strconv.Atoi(focal_length_str)
	check(err)
	return focal_length
}
