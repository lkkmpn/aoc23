package main

import (
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type race_data struct {
	time     int
	distance int
}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part1(string(input))
	part2(string(input))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part1(input string) {
	races := read_input_part1(input)

	product := 1

	for _, race := range races {
		product *= number_of_ways_to_beat_record(race)
	}

	fmt.Println("What do you get if you multiply these numbers together?", product)
}

func part2(input string) {
	race := read_input_part2(input)

	number := number_of_ways_to_beat_record(race)

	fmt.Println("How many ways can you beat the record in this one much longer race?", number)
}

func number_of_ways_to_beat_record(race race_data) int {
	// given a total race time `time`, the distance you can travel as function
	// of the time you hold the button `t` is
	//   d = (time - t) * t
	//     = -t^2 + t * time
	// you will win if you go farther than the current record holder, whose
	// distance is `distance`, so you will win if
	//   Δd = -t^2 + t * time - distance > 0
	// Δd = 0 is a quadratic equation, so the zeros are given by the quadratic
	// formula (a = -1, b = time, c = -distance)
	//   t_0 = (time ± √(time^2 - 4 * distance)) / 2
	time, distance := float64(race.time), float64(race.distance)
	t0p := (time + math.Sqrt(time*time-4*distance)) / 2.0
	t0n := (time - math.Sqrt(time*time-4*distance)) / 2.0

	// to satisfy Δd > 0 and the integer constraint on time, we need to find
	// how many integers exist _between_ t0n and t0p (t0n < t0p)
	lower_bound := int(math.Ceil(t0n))
	if t0n == math.Ceil(t0n) {
		// special case if t0n is an integer; i.e. if there is an integer t for
		// which Δd = 0
		lower_bound++
	}
	upper_bound := int(math.Floor(t0p))
	if t0p == math.Floor(t0p) {
		// special case if t0n is an integer; i.e. if there is an integer t for
		// which Δd = 0
		upper_bound--
	}

	return upper_bound - lower_bound + 1
}

func read_input_part1(input string) []race_data {
	lines := strings.Split(input, "\r\n")

	re_number := regexp.MustCompile(`\d+`)

	times := get_numbers_from_line(lines[0], re_number)
	distances := get_numbers_from_line(lines[1], re_number)

	if len(times) != len(distances) {
		panic("this shouldn't happen")
	}

	races := make([]race_data, len(times))
	for i := 0; i < len(times); i++ {
		races[i] = race_data{time: times[i], distance: distances[i]}
	}

	return races
}

func read_input_part2(input string) race_data {
	lines := strings.Split(strings.ReplaceAll(input, " ", ""), "\r\n")

	re_number := regexp.MustCompile(`\d+`)

	time := get_numbers_from_line(lines[0], re_number)[0]
	distance := get_numbers_from_line(lines[1], re_number)[0]

	return race_data{time: time, distance: distance}
}

func get_numbers_from_line(line string, re_number *regexp.Regexp) []int {
	numbers_str := re_number.FindAllString(line, -1)
	numbers := make([]int, len(numbers_str))

	for i, number_str := range numbers_str {
		number, err := strconv.Atoi(number_str)
		check(err)
		numbers[i] = number
	}

	return numbers
}
