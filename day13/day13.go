package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part(string(input), 0)
	part(string(input), 1) // if there is a smudge, exactly one character must be wrong
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part(input string, target_diff int) {
	patterns := read_input(input)

	sum := 0

	for _, pattern := range patterns {
		sum += summarize_pattern(pattern, target_diff)
	}

	if target_diff == 0 {
		fmt.Println("What number do you get after summarizing all of your notes?", sum)
	} else {
		fmt.Println("What number do you get after summarizing the new reflection line in each pattern in your notes?", sum)
	}
}

func summarize_pattern(pattern []string, target_diff int) int {
	// horizontal + vertical, assuming there is only one
	summary := 100*target_reflection_plane(pattern, target_diff) +
		target_reflection_plane(transpose_lines(pattern), target_diff)

	if summary == 0 {
		panic("no reflection found")
	}

	return summary
}

func target_reflection_plane(pattern []string, target_diff int) int {
	for i := 1; i < len(pattern); i++ {
		diff := reflected_diff(pattern, i)
		if diff == target_diff {
			return i
		}
	}

	return 0 // no reflection found
}

func reflected_diff(pattern []string, row_index int) int {
	// reflect across the horizontal line before `row_index`
	n_rows := len(pattern)
	rows_to_check := min(row_index, n_rows-row_index)
	total_diff := 0

	for i := 0; i < rows_to_check; i++ {
		top_row := pattern[row_index-i-1]
		bottom_row := pattern[row_index+i]
		diff := get_number_of_different_characters(top_row, bottom_row)
		total_diff += diff
	}

	return total_diff
}

func transpose_lines(lines []string) []string {
	required_length := len(lines[0])
	for _, line := range lines[1:] {
		if len(line) != required_length {
			panic("lines should be same length")
		}
	}

	transposed := make([]string, required_length)
	for _, column_idx := range make_range(0, required_length) {
		runes := make([]rune, len(lines))
		for i, row := range lines {
			runes[i] = []rune(row)[column_idx]
		}
		transposed[column_idx] = string(runes)
	}
	return transposed
}

func get_number_of_different_characters(a string, b string) int {
	if len(a) != len(b) {
		panic("strings should be of the same length")
	}

	count := 0

	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			count++
		}
	}

	return count
}

func read_input(input string) [][]string {
	pattern_strs := strings.Split(input, "\r\n\r\n")

	patterns := make([][]string, len(pattern_strs))

	for i, pattern_str := range pattern_strs {
		pattern_lines := strings.Split(strings.TrimSpace(pattern_str), "\r\n")
		patterns[i] = pattern_lines
	}

	return patterns
}

func make_range(min, max int) []int {
	// https://stackoverflow.com/a/39868255
	a := make([]int, max-min)
	for i := range a {
		a[i] = min + i
	}
	return a
}
