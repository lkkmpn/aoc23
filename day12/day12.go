package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type line_container struct {
	springs string
	groups  []int
}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part(string(input), 1)
	part(string(input), 5)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part(input string, repeat int) {
	lines := read_input(input, repeat)

	sum := 0
	cache := map[string]int{}

	for _, line := range lines {
		sum += check_line(line.springs, line.groups, cache)
	}

	if repeat == 1 {
		fmt.Println("What is the sum of those counts?", sum)
	} else {
		fmt.Println("What is the new sum of possible arrangement counts?", sum)
	}
}

func check_line(springs string, groups []int, cache map[string]int) int {
	// recursively iterate over the line of springs, solving possibilities as
	// we go

	// read cache
	key := cache_key(springs, groups)
	if v, ok := cache[key]; ok {
		return v
	}

	if len(springs) == 0 {
		// no more springs remaining to check
		if len(groups) == 0 {
			// no more groups remaining to check -> good
			return 1
		} else {
			// still groups remaining -> bad
			return 0
		}
	}

	switch springs[0] {
	// look at the next spring character
	case '.':
		// operational spring, consume and continue
		result := check_line(springs[1:], groups, cache)
		cache[key] = result
		return result

	case '?':
		// unknown spring, check both operational and damaged cases
		operational := "." + springs[1:]
		damaged := "#" + springs[1:]
		result := check_line(operational, groups, cache) +
			check_line(damaged, groups, cache)
		cache[key] = result
		return result

	case '#':
		// damaged spring
		if len(groups) == 0 {
			// no more groups of damaged springs remaining -> bad
			cache[key] = 0
			return 0
		}

		if len(springs) < groups[0] {
			// not enough springs are remaining -> bad
			cache[key] = 0
			return 0
		}

		if strings.Contains(springs[:groups[0]], ".") {
			// not enough damaged springs in a row -> bad
			cache[key] = 0
			return 0
		}

		if len(springs) == groups[0] {
			// these are the final springs
			if len(groups) == 1 {
				// this is also the final group -> good
				cache[key] = 1
				return 1
			} else {
				// there are more groups remaining -> bad
				cache[key] = 0
				return 0
			}
		}

		if springs[groups[0]] == '#' {
			// there is another damaged spring immediately after this group
			// -> bad
			cache[key] = 0
			return 0
		}

		// we assume the spring immediately after this group to be operational,
		// even if it is unknown in the string
		result := check_line(springs[groups[0]+1:], groups[1:], cache)
		cache[key] = result
		return result

	default:
		panic("this shouldn't happen")
	}
}

func read_input(input string, repeat int) []line_container {
	lines := strings.Split(strings.TrimSpace(input), "\r\n")

	processed_lines := make([]line_container, len(lines))

	for i, line := range lines {
		parts := strings.Split(line, " ")

		springs_copies := make([]string, repeat)
		groups_copies := make([]string, repeat)
		for j := range make_range(0, repeat) {
			springs_copies[j] = parts[0]
			groups_copies[j] = parts[1]
		}
		springs := strings.Join(springs_copies, "?")
		groups_str := strings.Join(groups_copies, ",")

		groups := strings_to_ints(strings.Split(groups_str, ","))
		processed_lines[i] = line_container{springs: springs, groups: groups}
	}

	return processed_lines
}

func strings_to_ints(strings []string) []int {
	ints := make([]int, len(strings))

	for i, str := range strings {
		v, err := strconv.Atoi(str)
		check(err)
		ints[i] = v
	}

	return ints
}

func cache_key(springs string, groups []int) string {
	key := springs
	for _, i := range groups {
		key += strconv.Itoa(i) + ","
	}
	return key
}

func make_range(min, max int) []int {
	// https://stackoverflow.com/a/39868255
	a := make([]int, max-min)
	for i := range a {
		a[i] = min + i
	}
	return a
}
