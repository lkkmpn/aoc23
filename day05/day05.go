package main

import (
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type map_item struct {
	dest_start   int
	source_start int
	length       int
}

type number_range struct {
	start  int
	length int
}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part1(string(input))
	part2(string(input))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part1(input string) {
	seeds, maps := read_input(input)

	lowest_location := math.MaxInt

	for _, seed := range seeds {
		location := seed_to_location(seed, maps)
		if location < lowest_location {
			lowest_location = location
		}
	}

	fmt.Println("What is the lowest location number that corresponds to any of the initial seed numbers?", lowest_location)
}

func part2(input string) {
	seed_input, maps := read_input(input)
	seed_ranges := seed_input_to_ranges(seed_input)

	lowest_location := math.MaxInt

	for _, seed_range := range seed_ranges {
		location_ranges := seed_range_to_location_ranges(seed_range, maps)
		for _, location_range := range location_ranges {
			if location_range.start < lowest_location {
				lowest_location = location_range.start
			}
		}
	}

	fmt.Println("What is the lowest location number that corresponds to any of the initial seed numbers?", lowest_location)
}

func seed_to_location(seed int, maps [][]map_item) int {
	current_value := seed

	for _, map_ := range maps {
		// any source numbers that aren't mapped correspond to the same
		// destination number, so we don't need to do anything special
		for _, item := range map_ {
			if item.source_start <= current_value &&
				current_value < item.source_start+item.length {
				// successfully mapped
				current_value += item.dest_start - item.source_start
				break
			}
		}
	}

	return current_value
}

func seed_range_to_location_ranges(seed_range number_range, maps [][]map_item) []number_range {
	current_objects := []number_range{seed_range}

	for _, map_ := range maps {
		// ranges to consider in the next level of maps
		// (i.e., after the seed-to-soil map, this contains the ranges of all
		// soil locations)
		new_objects := []number_range{}

		for _, map_item := range map_ {
			map_start := map_item.source_start
			map_end := map_item.source_start + map_item.length
			offset := map_item.dest_start - map_item.source_start

			// when looping over current_ranges, we might need to remove a
			// range from the list (when it is no longer required), but we
			// don't want to remove things from the list during the loop, so we
			// do it afterwards
			range_idxs_to_remove := []int{}

			for i, current_object := range current_objects {
				object_start := current_object.start
				object_end := current_object.start + current_object.length

				if object_end <= map_start || object_start >= map_end {
					// no overlap
					// map       ----
					// objects         ------------
					continue
				}

				if object_start >= map_start && object_end <= map_end {
					// the range of objects is fully contained in the map range
					// map       ------------------------
					// objects         ------------
					// translate       ^^^^^^^^^^^^
					new_object := number_range{
						start:  object_start + offset,
						length: current_object.length,
					}
					new_objects = append(new_objects, new_object)
					range_idxs_to_remove = append(range_idxs_to_remove, i)
				} else if object_start >= map_start && object_end > map_end {
					// the left side of the range of objects overlaps with the
					// map range
					// map       -----------
					// objects         ------------
					// translate       ^^^^^
					new_object := number_range{
						start:  object_start + offset,
						length: map_end - object_start,
					}
					new_objects = append(new_objects, new_object)

					// remove the overlapping part of the range of objects
					current_objects[i].start = map_end
					current_objects[i].length = object_end - map_end
				} else if object_start < map_start && object_end <= map_end {
					// the right side of the range of objects overlaps with the
					// map range
					// map                     ----------
					// objects         ------------
					// translate               ^^^^
					new_object := number_range{
						start:  map_start + offset,
						length: object_end - map_start,
					}
					new_objects = append(new_objects, new_object)

					// remove the overlapping part of the range of objects
					current_objects[i].length = map_start - object_start
				} else if object_start < map_start && object_end > map_end {
					// the range of objects fully contains the map range
					// map             ------------
					// objects   ------------------------
					// translate       ^^^^^^^^^^^^
					new_object := number_range{
						start:  map_start + offset,
						length: map_item.length,
					}

					// in this case, we need to split the current_range in two
					// we modify the object in-place for the left part, and add
					// a new one for the right part
					current_objects[i].length = map_start - object_start
					new_current_object := number_range{
						start:  map_end,
						length: object_end - map_end,
					}

					new_objects = append(new_objects, new_object)
					current_objects = append(current_objects, new_current_object)
				} else {
					// all cases should now be covered
					panic("this shouldn't happen")
				}
			}

			// efficient removal of items from a slice
			// https://stackoverflow.com/a/37335777
			for i := 0; i < len(range_idxs_to_remove); i++ {
				range_idx := range_idxs_to_remove[i]
				current_objects[range_idx] = current_objects[len(current_objects)-i-1]
			}
			current_objects = current_objects[:len(current_objects)-len(range_idxs_to_remove)]
		}

		current_objects = append(current_objects, new_objects...)
	}

	return current_objects
}

func read_input(input string) ([]int, [][]map_item) {
	lines := strings.Split(input, "\r\n")

	re_number := regexp.MustCompile(`\d+`)

	seeds := []int{}
	maps := [][]map_item{}

	// this assumes that the maps appear in order in the input file
	current_map := []map_item{}

	for _, line := range lines {
		if strings.Contains(line, "seeds") {
			// parse the seeds
			seeds = get_numbers_from_line(line, re_number)
		} else if line == "" {
			// append the current map to the list of maps
			if len(current_map) > 0 {
				maps = append(maps, current_map)
			}
		} else if strings.Contains(line, "map") {
			// start a new map
			current_map = []map_item{}
		} else {
			// map data
			numbers := get_numbers_from_line(line, re_number)
			if len(numbers) != 3 {
				panic("something went wrong")
			}
			item := map_item{
				dest_start:   numbers[0],
				source_start: numbers[1],
				length:       numbers[2],
			}
			current_map = append(current_map, item)
		}
	}

	return seeds, maps
}

func seed_input_to_ranges(seed_input []int) []number_range {
	ranges := make([]number_range, len(seed_input)/2)

	for i := 0; i < len(seed_input); i += 2 {
		ranges[i/2] = number_range{
			start:  seed_input[i],
			length: seed_input[i+1],
		}
	}

	return ranges
}

func get_numbers_from_line(line string, re_number *regexp.Regexp) []int {
	numbers_str := re_number.FindAllString(line, -1)
	numbers := make([]int, len(numbers_str))

	for i, number_str := range numbers_str {
		number, err := strconv.Atoi(number_str)
		check(err)
		numbers[i] = number
	}

	return numbers
}
