package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part1(string(input))
}

func part1(input string) {
	lines := strings.Split(input, "\r\n")

	part_number_sum := 0
	gear_ratio_sum := 0

	part_number_re := regexp.MustCompile(`\d+`) // digits
	symbol_re := regexp.MustCompile(`[^\.\d]`)  // not dot or digit
	asterisk_re := regexp.MustCompile(`\*`)     // gear

	asterisks := make(map[[2]int][]int)

	for line_idx, line := range lines {
		if line == "" {
			continue
		}

		// find part numbers
		matches := part_number_re.FindAllStringIndex(line, -1)

		for _, match := range matches {
			part_number, err := strconv.Atoi(line[match[0]:match[1]])
			check(err)

			// define rectangle around the part number
			left_idx := max(match[0]-1, 0)
			right_idx := min(match[1]+1, len(line))
			top_idx := max(line_idx-1, 0)
			btm_idx := min(line_idx+1, len(lines)-2) // ignore empty line at the end

			// loop over lines in rectangle
			for scan_line_idx := top_idx; scan_line_idx <= btm_idx; scan_line_idx++ {
				substr := lines[scan_line_idx][left_idx:right_idx]
				// check if there is a symbol in this line
				if symbol_re.FindAllString(substr, -1) != nil {
					// check if that symbol is a gear
					asterisk_match := asterisk_re.FindStringIndex(substr)
					if asterisk_match != nil {
						asterisk_char_idx := asterisk_match[0] + left_idx
						asterisk_coordinates := [2]int{scan_line_idx, asterisk_char_idx}
						asterisks[asterisk_coordinates] = append(asterisks[asterisk_coordinates], part_number)
					}

					part_number_sum += part_number
					break
				}
			}
		}
	}

	// check for gears (asterisks with two neighboring part numbers)
	for _, v := range asterisks {
		if len(v) == 2 {
			gear_ratio_sum += v[0] * v[1]
		}
	}

	fmt.Println("What is the sum of all of the part numbers in the engine schematic?", part_number_sum)
	fmt.Println("What is the sum of all of the gear ratios in your engine schematic?", gear_ratio_sum)
}
