package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	input, err := os.ReadFile("input.txt")

	if err != nil {
		panic(err)
	}

	part(string(input), 1)
	part(string(input), 2)
}

func part(input string, part int) {
	lines := strings.Split(input, "\n")

	var sum int = 0

	for _, line := range lines {
		if line == "" {
			continue
		}

		if part == 2 {
			words := []string{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}

			// words can overlap, but overlaps are allowed
			// hence, replace the word with a copy on both sides
			for i, word := range words {
				line = strings.ReplaceAll(line, word, fmt.Sprintf("%s%d%s", word, i+1, word))
			}
		}

		var i0 int = -1
		var i1 int = -1

		for _, char := range line {
			if char >= 48 && char <= 57 { // char is a digit
				if i0 == -1 { // set first digit
					i0 = int(char - 48)
				}
				i1 = int(char - 48) // always set second digit
			}
		}

		sum += i0*10 + i1
	}

	fmt.Println("What is the sum of all of the calibration values?", fmt.Sprint(sum))
}
