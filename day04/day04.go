package main

import (
	"fmt"
	"os"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part1(string(input))
	part2(string(input))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part1(input string) {
	lines := strings.Split(input, "\r\n")

	sum_points := 0

	for _, line := range lines {
		if line == "" {
			continue
		}

		count_winning := process_card(line)

		if count_winning > 0 {
			sum_points += 1 << (count_winning - 1)
		}
	}

	fmt.Println("How many points are they worth in total?", sum_points)
}

func part2(input string) {
	lines := strings.Split(input, "\r\n")

	num_cards_played := 0

	cards_to_play := map[int]int{}

	for _, card_idx := range make_range(0, len(lines)-2) {
		times_this_card := cards_to_play[card_idx] + 1

		count_winning := process_card(lines[card_idx])

		for _, new_card_idx := range make_range(card_idx+1, card_idx+count_winning) {
			cards_to_play[new_card_idx] += times_this_card
		}

		num_cards_played += times_this_card
	}

	fmt.Println("How many total scratchcards do you end up with?", num_cards_played)
}

func make_range(min, max int) []int {
	// https://stackoverflow.com/a/39868255
	a := make([]int, max-min+1)
	for i := range a {
		a[i] = min + i
	}
	return a
}

func process_card(input string) int {
	line_re := regexp.MustCompile(`(.+): (.+) \| (.+)`)
	match := line_re.FindStringSubmatch(input)

	winning_numbers := get_numbers(match[2])
	numbers_you_have := get_numbers(match[3])

	count_winning := 0
	for _, winning_number := range winning_numbers {
		if slices.Contains(numbers_you_have, winning_number) {
			count_winning++
		}
	}

	return count_winning
}

func get_numbers(input string) []int {
	number_re := regexp.MustCompile(`\d+`)
	matches := number_re.FindAllString(input, -1)
	numbers := []int{}
	for _, match := range matches {
		number, err := strconv.Atoi(match)
		check(err)
		numbers = append(numbers, number)
	}
	return numbers
}
