package main

import (
	"fmt"
	"math"
	"os"
	"strings"
)

type HeatmapContainer struct {
	heatmap map[Coordinates]int
	size_x  int
	size_y  int
}

type Coordinates struct {
	x int
	y int
}

type Direction struct {
	dx int
	dy int
}

func (c Coordinates) AddDirection(d Direction) Coordinates {
	return Coordinates{c.x + d.dx, c.y + d.dy}
}

var NORTH = Direction{0, -1}
var SOUTH = Direction{0, 1}
var WEST = Direction{-1, 0}
var EAST = Direction{1, 0}
var ANY_DIR = Direction{0, 0}

type PathSpecification struct {
	coordinates     Coordinates
	direction       Direction
	direction_steps int
}

type Node struct {
	visited  bool
	distance int
}

type Crucible struct {
	minimum_straight int
	maximum_straight int
}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part(string(input), Crucible{minimum_straight: 0, maximum_straight: 3})
	part(string(input), Crucible{minimum_straight: 4, maximum_straight: 10})
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part(input string, crucible Crucible) {
	heatmap_container := read_input(input)

	heat_loss := find_path(heatmap_container, crucible)

	fmt.Println("What is the least heat loss it can incur?", heat_loss)
}

func find_path(heatmap_container HeatmapContainer, crucible Crucible) int {
	// it's Dijkstra time

	nodes := map[PathSpecification]Node{}
	unvisited_path_specs := []PathSpecification{}

	current := PathSpecification{
		coordinates:     Coordinates{0, 0},
		direction:       ANY_DIR,
		direction_steps: 0,
	}
	final_coordinates := Coordinates{heatmap_container.size_x - 1, heatmap_container.size_y - 1}

	nodes[current] = Node{visited: true, distance: 0}

	for !(current.coordinates == final_coordinates && current.direction_steps >= crucible.minimum_straight) {
		neighbors := get_neighbors(current, heatmap_container, crucible)
		for _, neighbor := range neighbors {
			if !nodes[neighbor].visited {
				// calculate tentative distance
				distance_from_here := nodes[current].distance +
					heatmap_container.heatmap[neighbor.coordinates]

				// get distance calculated before (only if we already calculated this node before)
				known_distance := math.MaxInt
				_, ok := nodes[neighbor]
				if ok {
					known_distance = nodes[neighbor].distance
				}

				if distance_from_here < known_distance {
					nodes[neighbor] = Node{
						distance: distance_from_here,
						visited:  false,
					}
				}

				// if this is a new path specification, add it to the list (set) of unvisited
				if !ok {
					unvisited_path_specs = append(unvisited_path_specs, neighbor)
				}
			}
		}

		// mark current node as visited
		nodes[current] = Node{
			distance: nodes[current].distance,
			visited:  true,
		}

		// remove from unvisited list
		for i, path_specification := range unvisited_path_specs {
			if path_specification == current {
				unvisited_path_specs[i] = unvisited_path_specs[len(unvisited_path_specs)-1]
				unvisited_path_specs = unvisited_path_specs[:len(unvisited_path_specs)-1]
			}
		}

		// select next node to visit
		min_tentative_distance := math.MaxInt
		var next PathSpecification
		for _, path_spec := range unvisited_path_specs {
			node := nodes[path_spec]
			if node.distance < min_tentative_distance {
				min_tentative_distance = node.distance
				next = path_spec
			}
		}
		current = next
	}

	return nodes[current].distance
}

func get_neighbors(position PathSpecification, heatmap_container HeatmapContainer, crucible Crucible) []PathSpecification {
	valid_directions := []Direction{}

	// can we go north?
	if position.coordinates.y > 0 && // not out of bounds
		position.direction != SOUTH && // not going backwards
		(position.direction == NORTH || position.direction == ANY_DIR || // continuing direction
			position.direction_steps >= crucible.minimum_straight) && // allowed to turn
		!(position.direction == NORTH && position.direction_steps >= crucible.maximum_straight) { // allowed to turn
		valid_directions = append(valid_directions, NORTH)
	}

	// can we go south?
	if position.coordinates.y < heatmap_container.size_y-1 && // not out of bounds
		position.direction != NORTH && // not going backwards
		(position.direction == SOUTH || position.direction == ANY_DIR || // continuing direction
			position.direction_steps >= crucible.minimum_straight) && // allowed to turn
		!(position.direction == SOUTH && position.direction_steps >= crucible.maximum_straight) { // allowed to turn
		valid_directions = append(valid_directions, SOUTH)
	}

	// can we go west?
	if position.coordinates.x > 0 && // not out of bounds
		position.direction != EAST && // not going backwards
		(position.direction == WEST || position.direction == ANY_DIR || // continuing direction
			position.direction_steps >= crucible.minimum_straight) && // allowed to turn
		!(position.direction == WEST && position.direction_steps >= crucible.maximum_straight) { // allowed to turn
		valid_directions = append(valid_directions, WEST)
	}

	// can we go east?
	if position.coordinates.x < heatmap_container.size_x-1 && // not out of bounds
		position.direction != WEST && // not going backwards
		(position.direction == EAST || position.direction == ANY_DIR || // continuing direction
			position.direction_steps >= crucible.minimum_straight) && // allowed to turn
		!(position.direction == EAST && position.direction_steps >= crucible.maximum_straight) { // allowed to turn
		valid_directions = append(valid_directions, EAST)
	}

	// get new coordinates
	neighbors := []PathSpecification{}
	for _, valid_direction := range valid_directions {
		coordinates := position.coordinates.AddDirection(valid_direction)

		direction_steps := 1
		if valid_direction == position.direction {
			direction_steps += position.direction_steps
		}

		neighbors = append(neighbors, PathSpecification{
			coordinates:     coordinates,
			direction:       valid_direction,
			direction_steps: direction_steps,
		})
	}

	return neighbors
}

func read_input(input string) HeatmapContainer {
	lines := strings.Split(strings.TrimSpace(input), "\r\n")

	heatmap := map[Coordinates]int{}
	size_x, size_y := 0, 0

	for i, line := range lines {
		for j, char := range line {
			heatmap[Coordinates{j, i}] = int(char) - '0'
			if j >= size_x {
				size_x = j + 1
			}
		}
		if i >= size_y {
			size_y = i + 1
		}
	}

	return HeatmapContainer{
		heatmap: heatmap,
		size_x:  size_x,
		size_y:  size_y,
	}
}
