package main

import (
	"fmt"
	"os"
	"strings"
)

type Coordinates struct {
	x, y int
}

type Direction struct {
	dx, dy int
}

func (c Coordinates) AddDirection(d Direction) Coordinates {
	return Coordinates{c.x + d.dx, c.y + d.dy}
}

var NORTH = Direction{0, -1}
var SOUTH = Direction{0, 1}
var WEST = Direction{-1, 0}
var EAST = Direction{1, 0}

const EMPTY rune = '.'
const MIRROR_FORWARD rune = '/'
const MIRROR_BACKWARD rune = '\\'
const SPLITTER_VERTICAL rune = '|'
const SPLITTER_HORIZONTAL rune = '-'

type Tile struct {
	char  rune
	beams []Direction
}

type Beam struct {
	coordinates Coordinates
	direction   Direction
}

func main() {
	input, err := os.ReadFile("input.txt")
	check(err)

	part1(string(input))
	part2(string(input))
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func part1(input string) {
	layout := read_input(input)

	layout = process_beams(layout, Beam{
		coordinates: Coordinates{0, 0},
		direction:   EAST,
	})

	num_tiles := count_energized_tiles(layout)

	fmt.Println("How many tiles end up being energized?", num_tiles)
}

func part2(input string) {
	layout := read_input(input)

	max_num_tiles := 0

	for _, initial_beam := range get_initial_beams(layout) {
		new_layout := process_beams(layout, initial_beam)
		num_tiles := count_energized_tiles(new_layout)

		if num_tiles > max_num_tiles {
			max_num_tiles = num_tiles
		}
	}

	fmt.Println("How many tiles are energized in that configuration?", max_num_tiles)
}

func process_beams(layout_ [][]Tile, initial_beam Beam) [][]Tile {
	// make a copy of `layout_` to prevent
	// in-place operations on the original object
	layout := make([][]Tile, len(layout_))
	for i := range layout_ {
		layout[i] = make([]Tile, len(layout_[i]))
		copy(layout[i], layout_[i])
	}

	beams := []Beam{initial_beam}
	var split_beams []Beam

	for len(beams) > 0 {
		// process each unprocessed beam one-by-one
		beam := beams[0]
		beams = beams[1:]
		layout, split_beams = process_beam(layout, beam)
		beams = append(beams, split_beams...)
	}

	return layout
}

func process_beam(layout [][]Tile, beam Beam) ([][]Tile, []Beam) {
	split_beams := []Beam{}

	for {
		x, y := beam.coordinates.x, beam.coordinates.y

		// terminate if beam already exists in layout to prevent infinite loops
		for _, other_beam_direction := range layout[y][x].beams {
			if other_beam_direction == beam.direction {
				return layout, split_beams
			}
		}

		// add beam to layout
		layout[y][x].beams = append(layout[y][x].beams, beam.direction)

		// get next direction(s)
		directions := get_next_directions(layout[y][x], beam)
		beam.direction = directions[0]
		if len(directions) > 1 {
			// beam split
			split_beams = append(split_beams, Beam{
				coordinates: beam.coordinates,
				direction:   directions[1],
			})
		}

		// advance beam coordinates
		new_coordinates := beam.coordinates.AddDirection(beam.direction)

		// terminate if beam leaves layout
		if new_coordinates.x < 0 || new_coordinates.x >= len(layout[0]) ||
			new_coordinates.y < 0 || new_coordinates.y >= len(layout) {
			return layout, split_beams
		}

		beam.coordinates = new_coordinates
	}
}

func get_next_directions(tile Tile, beam Beam) []Direction {
	switch tile.char {
	case EMPTY:
		return []Direction{beam.direction}
	case MIRROR_FORWARD:
		return []Direction{{-beam.direction.dy, -beam.direction.dx}}
	case MIRROR_BACKWARD:
		return []Direction{{beam.direction.dy, beam.direction.dx}}
	case SPLITTER_VERTICAL:
		if beam.direction.dx == 0 {
			return []Direction{beam.direction}
		} else {
			return []Direction{NORTH, SOUTH}
		}
	case SPLITTER_HORIZONTAL:
		if beam.direction.dy == 0 {
			return []Direction{beam.direction}
		} else {
			return []Direction{WEST, EAST}
		}
	default:
		panic("this shouldn't happen")
	}
}

func count_energized_tiles(layout [][]Tile) int {
	num_tiles := 0

	for _, row := range layout {
		for _, tile := range row {
			if len(tile.beams) > 0 {
				num_tiles++
			}
		}
	}

	return num_tiles
}

func get_initial_beams(layout [][]Tile) []Beam {
	initial_beams := []Beam{}

	for i := 0; i < len(layout); i++ {
		// left and right edge
		initial_beams = append(initial_beams,
			Beam{
				coordinates: Coordinates{0, i},
				direction:   EAST,
			},
			Beam{
				coordinates: Coordinates{len(layout[i]) - 1, i},
				direction:   WEST,
			},
		)
	}

	for i := 0; i < len(layout[0]); i++ {
		// top and bottom edge
		initial_beams = append(initial_beams,
			Beam{
				coordinates: Coordinates{i, 0},
				direction:   SOUTH,
			},
			Beam{
				coordinates: Coordinates{len(layout) - 1, i},
				direction:   NORTH,
			},
		)
	}

	return initial_beams
}

func read_input(input string) [][]Tile {
	lines := strings.Split(strings.TrimSpace(input), "\r\n")

	layout := [][]Tile{}
	for _, line := range lines {
		row := []Tile{}
		for _, char := range line {
			row = append(row, Tile{char: char})
		}
		layout = append(layout, row)
	}

	return layout
}
